'use strict';

var self = getById;
module.exports = self;

var async = require('async');
var customers = require('../Models/customers.js');

function getById(req, res) {
  var bag = {
    inputParams: req.params,
    resBody: {}
  };

  bag.who = util.format('customers|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _getCustomerById.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  if (!bag.inputParams.customerId)
    return next(
      'Route parameter not found :customerId'
    );

  return next();
}

function _getCustomerById(bag, next) {
  var who = bag.who + '|' + _getCustomerById.name;
  logger.debug(who, 'Inside');

  var query = {
    where: {
      id: bag.inputParams.customerId
    }
  };

  customers.findOne(query).asCallback(
    function (err, customer) {
      if (err)
        return next(
          'customers.findOne failed with err: ' + err.message
        );

      bag.resBody = customer;
      return next();
    }
  );
}
