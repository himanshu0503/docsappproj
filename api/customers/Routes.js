'use strict';

module.exports = customers;
var validateUser = require('../validators/validateUser.js');

function customers(app) {
  app.get('/api/customers/:customerId', validateUser,
    require('./getById.js'));
  app.post('/api/customers', validateUser, require('./post.js'));
}
