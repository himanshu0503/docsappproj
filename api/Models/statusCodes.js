'use strict';
var Sequelize = require('sequelize');

var statusCodes = global.sequelize.define('statusCodes', {
  code: {
    type: Sequelize.INTEGER,
    allowNull: false,
    primaryKey: true,
    field: 'code',
  },
  name: {
    type: Sequelize.STRING(255),
    field: 'name',
    allowNull: false
  }
}, {
  tableName: 'statusCodes',
  indexes: [
    {
      name: 'statusCodeCodeU',
      fields: ['code'],
      unique: true
    }
  ]
});

module.exports = statusCodes;
