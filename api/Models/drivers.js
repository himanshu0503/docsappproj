'use strict';
var Sequelize = require('sequelize');

var statusCodes = require('./statusCodes.js');

var drivers = global.sequelize.define('drivers', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(24),
    field: 'name',
    allowNull: true
  },
  email: {
    type: Sequelize.STRING(24),
    field: 'email',
    allowNull: true
  },
  phoneNumber: {
    type: Sequelize.INTEGER,
    unique: true,
    allowNull: false,
    field: 'phoneNumber',
  },
  details: {
    type: Sequelize.STRING(256),
    field: 'details',
    allowNull: true
  },
  statusCode: {
    type: Sequelize.INTEGER,
    field: 'statusCode',
    allowNull: false
  }
}, {
  tableName: 'drivers',
  indexes: [
    {
      name: 'driverIdU',
      fields: ['id'],
      unique: true
    }
  ]
});

module.exports = drivers;
