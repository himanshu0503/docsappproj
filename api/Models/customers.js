'use strict';
var Sequelize = require('sequelize');

var customers = global.sequelize.define('customers', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  name: {
    type: Sequelize.STRING(24),
    field: 'name',
    allowNull: true
  },
  email: {
    type: Sequelize.STRING(24),
    field: 'email',
    allowNull: true
  }
}, {
  tableName: 'customers',
  indexes: [
    {
      name: 'customerIdU',
      fields: ['id'],
      unique: true
    }
  ]
});

module.exports = customers;
