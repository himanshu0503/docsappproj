'use strict';
var Sequelize = require('sequelize');
var statusCodes = require('./statusCodes.js');
var drivers = require('./drivers.js');
var customers = require('./customers.js');

var cabRequests = global.sequelize.define('cabRequests', {
  id: {
    type: Sequelize.INTEGER,
    primaryKey: true,
    allowNull: false,
    autoIncrement: true
  },
  customerId: {
    type: Sequelize.STRING(24),
    field: 'customerId',
    allowNull: false
  },
  driverId: {
    type: Sequelize.STRING(24),
    field: 'driverId',
    allowNull: true
  },
  requestProcessingTime: {
    type: Sequelize.DATE,
    field: 'requestProcessingTime',
    allowNull: true
  },
  statusCode: {
    type: Sequelize.INTEGER,
    field: 'statusCode',
    allowNull: false
  }
}, {
  indexes: [
    {
      name: 'cabRequestIdU',
      fields: ['id'],
      unique: true
    },
    {
      name: 'cabReqCustomerIdI',
      fields: ['customerId']
    },
    {
      name: 'cabReqDriverIdI',
      fields: ['driverId']
    }
  ]
});

module.exports = cabRequests;
