### table - StatusCodes
- "code" INT PRIMARY KEY NOT NULL
- "name" VARCHAR(12) NOT NULL 

#### Indexes
- statusCodeCodeU(code)

### table - Customers
- "id" INT PRIMARY KEY NOT NULL
- "name" VARCHAR(24)
- "email" VARCHAR(12)

#### Indexes 
- customerIdU(id)

### table - Drivers
- "id" INT PRIMARY KEY NOT NULL
- "name" VARCHAR(24)  
- "email" VARCHAR(12)
- "statusCode" INT NOT NULL
- "currentCustomerId" INT
- "phoneNumber" INT NOT NULL
- "details"  VARCHAR(24) INT NOT NULL

#### Indexes 
- drivereIdU(id)

#### Foreign Keys
- currentCustomerId belongs to customer.id
- statusCode belongs to statusCodes.id

### table - CabRequests
- "id" INT PRIMARY KEY NOT NULL
- "customerId" INT NOT NULL
- "driverId" INT
- "statusCode" INT NOT NULL
- "requestProcessingTime" DATE

#### Indexes
- cabRequestIdU(id), 
- cabReqCustomerId(customerId),
- cabReqDriverId(driverId)

#### Foreign Keys
- customerId belongs to customer.id
- driverId belongs to driver.id
- statusCode belongs to statusCodes.id
