### Routes - StatusCodes

GET /statusCodes
reqBody
```javascript
{}
```
resBody
```javascript
[
  {
    "code": 10,
    "name": waiting
  },
  ...
]
```

###  Routes - Customers

#### POST /customers
reqBody
```javascript
{
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com"
}
```

resBody
```javascript
{
  "id": 1
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com"
}
```

#### GET /customers/:customerId

req.inputParams - `customerId`

resBody
```javascript
{
  "id": 1
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com"
}
```

### Routes - Drivers


#### POST /drivers
reqBody
```javascript
{
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com",
  "phoneNumber": 1234567890,
  "details": "cabnumber - KA05-JP-2341"
}
```

resBody
```javascript
{
  "id": 1
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com",
  "statusCode": 10,
  "phoneNumber": 1234567890,
  "details": "cabnumber - KA05-JP-2341"
}
```

#### GET /drivers/:driverId

req.inputParams - `driverId`

resBody
```javascript
{
  "id": 1
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com",
  "statusCode": 10
}
```

#### PUT /drivers/:driverId

reqBody
```javascript
{
  "statusCode": 10
}
```

resBody
```javascript
{
  "id": 1
  "name": "himanshu",
  "email": "hemanshu0503@gmail.com",
  "statusCode": 10
}
```


### Routes - CabRequests

#### POST /cabRequests
reqBody
```javascript
{
  "customerId": 1
}
```

resBody
```javascript
{
  "id": 1
  "customerId": 1,
  "driverId": null,
  "requestProcessingTime": null,
  "statusCode": 10,
  "createdAt": 2017-02-09 10:14:47.408+00,
  "updatedAt": 2017-02-09 10:14:47.408+00
}
```

#### POST /cabRequests/:cabRequestId/accept

req.inputParams - `cabRequestId`

resBody
```javascript
{
  "id": 1
  "customerId": 1,
  "driverId": 1,
  "statusCode": 10,
  "requestProcessingTime": 2017-02-09 10:14:47.408+00,
  "createdAt": 2017-02-09 10:14:47.408+00,
  "updatedAt": 2017-02-09 10:14:47.408+00
}
```

#### GET /cabRequests?

req.query -
no query - returns all the data
customerIds = returns data for specific customerIds
driverIds = returns data for specific driverIds
cabRequestIds = returns data for specific cabRequestIds
statusCodes = returns data for specific statusCodes

resBody
```javascript
[
  {
    "id": 1
    "customerId": 1,
    "driverId": 1,
    "statusCode": 10,
    "requestProcessingTime": 2017-02-09 10:14:47.408+00,
    "createdAt": 2017-02-09 10:14:47.408+00,
    "updatedAt": 2017-02-09 10:14:47.408+00
  }
]
```