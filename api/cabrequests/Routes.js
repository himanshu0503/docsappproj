'use strict';

module.exports = cabRequests;
var validateUser = require('../validators/validateUser.js');

function cabRequests(app) {
  app.get('/api/cabRequests', validateUser, require('./getS.js'));
  app.post('/api/cabRequests', validateUser, require('./post.js'));
  app.post('/api/cabRequests/:cabRequestId/accept', validateUser,
    require('./acceptById.js'));
  app.post('/api/cabRequests/:cabRequestId/finish', validateUser,
    require('./finishById.js'));
}
