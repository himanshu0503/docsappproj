'use strict';

var self = getS;
module.exports = self;

var async = require('async');
var cabRequests = require('../Models/cabRequests.js');
var _ = require('underscore');

function getS(req, res) {
  var bag = {
    resBody: [],
    reqQuery: req.query
  };

  bag.who = util.format('cabRequests|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _constructQuery.bind(null, bag),
    _getCabRequests.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  return next();
}

function _constructQuery(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  var qy = {
    where: {}
  };
  var rq = bag.reqQuery;

  if (rq.customerIds) {
    qy.where.customerId = {
      $in: rq.customerIds.split(',')
    };
  }

  if (rq.driverIds) {
    qy.where.driverId = {
      $in: rq.driverIds.split(',')
    };
  }

  if (rq.statusCodes) {
    qy.where.statusCode = {
      $in: rq.statusCodes.split(',')
    };
  }

  bag.query = qy;
  return next();
}

function _getCabRequests(bag, next) {
  var who = bag.who + '|' + _getCabRequests.name;
  logger.debug(who, 'Inside');

  cabRequests.findAll(bag.query).asCallback(
    function (err, cabRequests) {
      if (err)
        return next(
          'cabRequests.findAll failed with err: ' + err.message
        );

      bag.resBody = cabRequests;
      return next();
    }
  );
}
