'use strict';

var self = finishById;
module.exports = self;

var async = require('async');
var cabRequests = require('../Models/cabRequests.js');
var Adapter = require('../../common/APIAdapter.js');

function finishById(req, res) {
  var bag = {
    resBody: {},
    inputParams: req.params,
    reqBody: req.body
  };

  bag.who = util.format('cabRequests|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _getCabRequestId.bind(null, bag),
    _put.bind(null, bag),
    _updateDriverStatus.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  if (!bag.inputParams.cabRequestId)
    return next('no cabRequestId provided, erroring');

  return next();
}

function _getCabRequestId(bag, next) {
  var who = bag.who + '|' + _getCabRequestId.name;
  logger.debug(who, 'Inside');

  var query = {
    where: {
      id: bag.inputParams.cabRequestId
    }
  };

  cabRequests.findOne(query).asCallback(
    function (err, cabRequest) {
      if (err)
        return next(
          'cabRequests.findOne failed with err: ' + err.message
        );

      bag.cabRequest = cabRequest;

      if (bag.cabRequest.statusCode === 30)
        return next('already finished the trip');

      return next();
    }
  );
}

function _put(bag, next) {
  var who = bag.who + '|' + _put.name;
  logger.debug(who, 'Inside');

  var update = {
    statusCode: 30
  };

  bag.cabRequest.update(update).asCallback(
    function (err, cabRequest) {
      if (err)
        return next(
            'cabRequest.update failed with err: ' + err.message
        );

      bag.reqBody = cabRequest;
      return next();
    }
  );
}

function _updateDriverStatus(bag, next) {
  var who = bag.who + '|' + _put.name;
  logger.debug(who, 'Inside');

  var adapter = new Adapter();

  var update = {
    statusCode: 40
  };

  adapter.putDriverById(bag.reqBody.driverId, update,
    function(err) {
      if (err)
        return next('failed to update driver');

      return next();
    }
  );
}