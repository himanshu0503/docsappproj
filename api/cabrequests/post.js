'use strict';

var self = post;
module.exports = self;

var async = require('async');
var cabRequests = require('../Models/cabRequests.js');

function post(req, res) {
  var bag = {
    resBody: {},
    reqBody: req.body
  };

  bag.who = util.format('cabRequests|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _post.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  if (!bag.reqBody.customerId)
    return next('no customerId provided, erroring');

  return next();
}

function _post(bag, next) {
  var who = bag.who + '|' + _post.name;
  logger.debug(who, 'Inside');

  //TODO: Fetch statusCode by making a call to
  // GET statusCodes

  var newCabRequest = {
    customerId: bag.reqBody.customerId,
    statusCode: 10
  };

  cabRequests.create(newCabRequest).asCallback(
    function (err, cabRequest) {
      if (err)
        return next(
            'cabRequests.create failed with error: ' + err.message
        );

      bag.resBody = cabRequest;
      return next();
    }
  );
}
