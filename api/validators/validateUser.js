'use strict';
var self = validateUser;
module.exports = self;


//Middleware to validateUser
function validateUser(req, res, next) {
  var bag = {
    req: req,
    reqQuery: req.query || {},
    inputParams: req.params || {},
    reqPath: req.route.path,
  };

  bag.who = util.format('%s|%s|%s', bag.reqPath, bag.reqMethod, self.name);
  logger.debug(bag.who, 'Started');

  return next();

}
