'use strict';

module.exports = statusCodes;
var validateUser = require('../validators/validateUser.js');

function statusCodes(app) {
  app.get('/api/statusCodes', validateUser, require('./getS.js'));
}
