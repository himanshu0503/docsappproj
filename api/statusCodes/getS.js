'use strict';

var self = getS;
module.exports = self;

var async = require('async');
var statusCodes = require('../Models/statusCodes.js');

function getS(req, res) {
  var bag = {
    resBody: []
  };

  bag.who = util.format('statusCodes|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _getStatusCodes.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _getStatusCodes(bag, next) {
  var who = bag.who + '|' + _getStatusCodes.name;
  logger.debug(who, 'Inside');

  statusCodes.findAll().asCallback(
    function (err, statusCodes) {
      if (err)
        return next(
          'statusCodes.findAll failed with err: ' + err.message
        );

      bag.resBody = statusCodes;
      return next();
    }
  );
}
