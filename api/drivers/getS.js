'use strict';

var self = getS;
module.exports = self;

var async = require('async');
var drivers = require('../Models/drivers.js');

function getS(req, res) {
  var bag = {
    resBody: [],
    reqQuery: req.query
  };

  bag.who = util.format('drivers|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _getDrivers.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  return next();
}

function _getDrivers(bag, next) {
  var who = bag.who + '|' + _getDrivers.name;
  logger.debug(who, 'Inside');

  drivers.findAll({}).asCallback(
    function (err, drivers) {
      if (err)
        return next(
          'drivers.findAll failed with err: ' + err.message
        );

      bag.resBody = drivers;
      return next();
    }
  );
}
