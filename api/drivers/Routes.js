'use strict';

module.exports = drivers;
var validateUser = require('../validators/validateUser.js');

function drivers(app) {
  app.get('/api/drivers/:driverId', validateUser,
    require('./getById.js'));
  app.get('/api/drivers', validateUser,
    require('./getS.js'));
  app.post('/api/drivers', validateUser, require('./post.js'));
  app.put('/api/drivers/:driverId', validateUser, require('./putById.js'));
}
