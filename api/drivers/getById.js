'use strict';

var self = getById;
module.exports = self;

var async = require('async');
var drivers = require('../Models/drivers.js');

function getById(req, res) {
  var bag = {
    inputParams: req.params,
    resBody: {}
  };

  bag.who = util.format('drivers|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _getDriverById.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  if (!bag.inputParams.driverId)
    return next(
      'Route parameter not found :driverId'
    );

  return next();
}

function _getDriverById(bag, next) {
  var who = bag.who + '|' + _getDriverById.name;
  logger.debug(who, 'Inside');

  var query = {
    where: {
      id: bag.inputParams.driverId
    }
  };

  drivers.findOne(query).asCallback(
    function (err, driver) {
      if (err)
        return next(
          'driver.findOne failed with err: ' + err.message
        );

      bag.resBody = driver;
      return next();
    }
  );
}
