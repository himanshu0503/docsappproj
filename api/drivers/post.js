'use strict';

var self = post;
module.exports = self;

var async = require('async');
var drivers = require('../Models/drivers.js');

function post(req, res) {
  var bag = {
    resBody: {},
    reqBody: req.body
  };

  bag.who = util.format('drivers|%s', self.name);
  logger.info(bag.who, 'Starting');

  async.series([
    _checkInputParams.bind(null, bag),
    _post.bind(null, bag)
  ],
    function (err) {
      logger.info(bag.who, 'Completed');
      if (err)
        return res.status(500).json(err);

      res.status(200).json(bag.resBody);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  if (!bag.reqBody.phoneNumber)
    return next('no phone number provided, erroring');

  return next();
}

function _post(bag, next) {
  var who = bag.who + '|' + _post.name;
  logger.debug(who, 'Inside');

  var newDriver = {
    name: bag.reqBody.name,
    email: bag.reqBody.email,
    phoneNumber: bag.reqBody.phoneNumber,
    details: bag.reqBody.details,
    statusCode: 40
  };

  drivers.create(newDriver).asCallback(
    function (err, driver) {
      if (err)
        return next(
            'drivers.create failed with error: ' + err.message
        );

      bag.resBody = driver;
      return next();
    }
  );
}
