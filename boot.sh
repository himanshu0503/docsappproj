#!/bin/bash -e

cd /home/app/docsappproj

forever -w -v --minUptime 1000 --spinSleepTime 1000 app.js
