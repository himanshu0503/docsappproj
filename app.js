'use strict';

process.title = 'app';
module.exports = init;

require('./common/logger.js');

var async = require('async');
var _ = require('underscore');
var express = require('express');
var path = require('path');
var glob = require('glob');
var Sequelize = require('sequelize');

global.util = require('util');
global.config = {};

process.on('uncaughtException',
  function (err) {
    _logErrorAndExit('uncaughtException thrown:', err);
  }
);

function init() {
  var bag = {
    app: global.app,
    env: process.env,
    config: {
      appPort: 50000
    }
  };

  bag.who = util.format('app|%s', init.name);

  async.series([
      _createExpressApp.bind(null, bag),
      _initializeDatabaseConfig.bind(null, bag),
      _initializeSequelize.bind(null, bag),
      _initializeRoutes.bind(null, bag),
      _startListening.bind(null, bag),
      _setLogLevel.bind(null, bag),
      _loadDrivers.bind(null, bag),
      _loadDriverCron.bind(null, bag)
    ],
    function (err) {
      if (err) {
        logger.error('Could not initialize api app: ' +
          util.inspect(err));
      } else {
        logger.info(bag.who, 'Completed');
        global.app = bag.app;
        module.exports = global.app;
      }
    }
  );
}

function _createExpressApp(bag, next) {
  try {
    var app = express();
    app.use(require('body-parser').json({limit: '10mb'}));
    // Views config
    app.engine('html', require('ejs').renderFile);
    app.set('view engine', 'html');

    app.use(express.static(path.join(__dirname, './static')));

    bag.app = app;
    return next();
  } catch (err) {
    _logErrorAndExit('Uncaught Exception thrown from createExpressApp.', err);
  }
}

function _initializeDatabaseConfig(bag, next) {
  var who = 'app.js|' + _initializeDatabaseConfig.name;
  logger.debug(who, 'Inside');

  var configErrors = [];

  if (bag.env.DBNAME)
    bag.config.dbName = bag.env.DBNAME;
  else
    configErrors.push('DBNAME is not defined');

  if (bag.env.DBUSERNAME)
    bag.config.dbUsername = bag.env.DBUSERNAME;
  else
    configErrors.push('DBUSERNAME is not defined');

  if (bag.env.DBPASSWORD)
    bag.config.dbPassword = bag.env.DBPASSWORD;
  else
    configErrors.push('DBPASSWORD is not defined');

  if (bag.env.DBHOST)
    bag.config.dbHost = bag.env.DBHOST;
  else
    configErrors.push('DBHOST is not defined');

  if (bag.env.DBPORT)
    bag.config.dbPort = bag.env.DBPORT;
  else
    configErrors.push('DBPORT is not defined');

  if (bag.env.DBDIALECT)
    bag.config.dbDialect = bag.env.DBDIALECT;
  else
    configErrors.push('DBDIALECT is not defined');

  if (configErrors.length)
    next(configErrors);
  else next();
}

function _initializeSequelize(bag, next) {
  var who = 'api.app.js|' + _initializeSequelize.name;
  logger.debug(who, 'Inside');

  var sequelizeOptions = {
    host: bag.config.dbHost,
    dialect: bag.config.dbDialect,
  };

  var sequelize = new Sequelize(
    bag.config.dbName,
    bag.config.dbUsername,
    bag.config.dbPassword,
    sequelizeOptions);

  global.sequelize = sequelize;

  // Initialize all the models
  glob.sync('./api/Models/*.js').forEach(
    function (schemaPath) {
      logger.debug(who, 'Initializing schema file', schemaPath);
      require(schemaPath);
    }
  );

  sequelize.sync().asCallback(
    function (err) {
      if (err)
        return next('Failed to sync sequelize with err:' + err);

      logger.debug('SEQUELIZE: Synced successfully');
      return next();
    }
  );
}

function _initializeRoutes(bag, next) {
  var who = bag.who + '|' + _initializeRoutes.name;
  logger.debug(who, 'Inside');

  glob.sync('./api/**/*Routes.js').forEach(
    function(routeFile) {
      require(routeFile)(bag.app);
    }
  );

  require('./Routes.js')(bag.app);

  return next();
}

function _startListening(bag, next) {
  var who = bag.who + '|' + _startListening.name;
  logger.debug(who, 'Inside');

  var appPort = bag.config.appPort;

  bag.app.listen(appPort, '0.0.0.0',
    function (err) {
      if (err)
        return next(err);
      logger.info('App listening on %s.', appPort);
      logger.info('Visit.', appPort);
      return next();
    }
  );
}

function _setLogLevel(bag, next) {
  var who = bag.who + '|' + _setLogLevel.name;
  logger.debug(who, 'Inside');

  var loggerConfig = {};
  loggerConfig.runMode = 'production';

  logger.debug('Setting log level as ' + loggerConfig.runMode);
  logger.configLevel(loggerConfig);

  return next();
}

function _loadDrivers(bag, next) {
  var who = bag.who + '|' + _loadDrivers.name;
  logger.debug(who, 'Inside');

  var drivers = require('./api/Models/drivers.js');
  drivers.findAll({}).asCallback(
    function (err, drivers) {
      if (err)
        return next(
          'drivers.findOne failed with err: ' + err.message
        );

      if (_.isEmpty(drivers)) {
        logger.warn('No drivers found');
      }

      bag.drivers = drivers;
      return next();
    }
  );

}

function _loadDriverCron(bag, next) {
  var who = bag.who + '|' + _loadDriverCron.name;
  logger.debug(who, 'Inside');

  async.eachSeries(bag.drivers,
    function(driver, nextDriver) {
      require('./workers/driverCron'+ driver.id + '.js')(driver.id);
      setTimeout(
        function(){
          nextDriver();
        },
        1000);
    },
    function() {
      return next();
    }
  );
}

function _logErrorAndExit(message, err) {
  logger.error(message);

  if (err && err.message)
    logger.error(err.message);

  if (err && err.stack)
    logger.error(err.stack);

  setTimeout(
    function () {
      process.exit(1);
    },
    3000
  );
}

if (require.main === module)
  init();

