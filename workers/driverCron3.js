'use strict';

var self = driverCron3;
module.exports = self;

//TODO: This should ideally be replaced with a broadcast queue but due to time
// constraint it wasn't done

var DELAY = 5 * 1000; // 5 seconds
var TRIP_COMPLETE_MS = 5* 60 * 1000; //5 mins
var Adapter = require('../common/APIAdapter.js');
var async = require('async');
var _ = require('underscore');

function driverCron3(driverId) {
  var bag = {
    driverId: driverId,
    adapter: new Adapter(),
    isDriverFree: true,
    cabReqId: null
  };

  bag.who = util.format('workers|driverId:%s|%s', driverId, self.name);
  logger.error(bag.who, 'Started');

  async.series([
      _checkInputParams.bind(null, bag),
      _isDriverFree.bind(null, bag),
      _getOpenCabRequests.bind(null, bag),
      _acceptCabRequest.bind(null, bag),
      _finishCabRequest.bind(null, bag)
    ],
    function (err) {
      if (err)
        logger.error(err);

      logger.verbose(bag.who,
        util.format('completed, going to sleep for %s seconds',
          DELAY/1000)
      );
      setTimeout(
        function() {
          driverCron3(driverId);
        },
        DELAY);
    }
  );
}

function _checkInputParams(bag, next) {
  var who = bag.who + '|' + _checkInputParams.name;
  logger.debug(who, 'Inside');

  return next();
}

function _isDriverFree(bag, next) {
  var who = bag.who + '|' + _isDriverFree.name;
  logger.debug(who, 'Inside');

  bag.adapter.getDriverById(bag.driverId,
    function(err, driver) {
      if (err)
        return next(err);

      if (driver.statusCode === 50)
        bag.isDriverFree = false;

      return next();
    }
  );
}

function _getOpenCabRequests(bag, next) {
  if (!bag.isDriverFree) return next();

  var who = bag.who + '|' + _getOpenCabRequests.name;
  logger.debug(who, 'Inside');

  var query = 'statusCodes=10';

  bag.adapter.getCabRequestsByQuery(query,
    function(err, cabReqs) {
      if (err)
        return next(err);
      if (_.isEmpty(cabReqs))
        return next('no cab reqs right now, sleeping for 5 secs');
      bag.openCabReqs = cabReqs;
      return next();
    }
  );
}

function _acceptCabRequest(bag, next) {
  if (!bag.isDriverFree) return next();

  var who = bag.who + '|' + _acceptCabRequest.name;
  logger.debug(who, 'Inside');

  async.eachSeries(bag.openCabReqs,
    function(cabReq, nextCabReq) {
      if (!bag.isDriverFree) return nextCabReq();

      var body = {
        driverId: bag.driverId
      };

      bag.adapter.acceptCabRequest(cabReq.id, body,
        function(err) {
          if (err)
            return nextCabReq();

          bag.cabReqId = cabReq.id;

          logger.error('driver:' + bag.driverId +
            ' has been assigned to cabReq:' + bag.cabReqId);
          bag.isDriverFree = false;
          return nextCabReq();
        }
      );
    },
    function() {
      if (!bag.isDriverFree) {
        setTimeout(
          function() {
            return next();
          },
          TRIP_COMPLETE_MS
        );
      } else {
        return next();
      }
    }
  );
}

function _finishCabRequest(bag, next) {
  if (bag.isDriverFree) return next();
  if (!bag.cabReqId) return next();

  var who = bag.who + '|' + _finishCabRequest.name;
  logger.debug(who, 'Inside');

  bag.adapter.finishCabRequest(bag.cabReqId, {},
    function(err) {
      if (err)
        return next(err);

      logger.error('driver:' + bag.driverId +
        ' has finished cabReq:' + bag.cabReqId);
      return next();
    }
  );
}