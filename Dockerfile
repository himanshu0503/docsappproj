FROM drydock/microbase:master

ADD . /home/app/docsappproj

RUN cd /home/app/docsappproj && npm install

RUN mkdir -p /var/run/app/logs

ENTRYPOINT ["/home/app/docsappproj/boot.sh"]

EXPOSE 50000
