(function () {
  'use strict';



  docsappproj.service('docsAppProjService', ['$http', '$rootScope',
    docsAppProjService
  ]);

  var timeout = 120000;

  function docsAppProjService($http, $rootScope) {
    var BASE_URL = 'http://localhost:50000';
    function handler(promise, callback) {
      var startTime = Date.now();
      if (callback)
        promise
          .success(function (data) {
            callback(null, data);
          })
          .error(function (data) {
            if (!data) {
              var endTime = Date.now();
              if ((endTime - startTime) >= timeout)
                return callback('Request timed out', null);
              else
                return callback('Request failed', null);
            }

            callback(data, null);
          });

      return promise;
    }
    return {
      // We're getting the login token here because it's not set until login.
      get: function (path, callback) {

        var promise = $http.get(BASE_URL + path, {
          headers: {
            Authorization: 'apiToken ' + $rootScope._r.loginToken
          },
          timeout: timeout
        });
        return handler(promise, callback);
      },
      put: function (path, body, callback) {
        var promise = $http.put(BASE_URL + path, body, {
          headers: {
            Authorization: 'apiToken ' + $rootScope._r.loginToken
          },
          timeout: timeout
        });
        return handler(promise, callback);
      },
      post: function (path, body, callback) {
        var promise = $http.post(BASE_URL + path, body, {
          headers: {
            Authorization: 'apiToken ' + $rootScope._r.loginToken
          },
          timeout: timeout
        });
        return handler(promise, callback);
      },
      delete: function (path, body, callback) {
        var promise = $http.delete(BASE_URL + path, {
          headers: {
            Authorization: 'apiToken ' + $rootScope._r.loginToken,
            'Content-Type': 'application/json;charset=utf-8'
          },
          data: body,
          timeout: timeout
        });
        return handler(promise, callback);
      }

    };
  }
}());
