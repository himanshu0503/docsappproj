(function () {
  'use strict';

  /** apiAdapter
   *  Used for calling API routes
   */

  docsappproj.service('apiAdapter', ['docsAppProjService',
    apiAdapter
  ]);

  function apiAdapter(docsAppProjService) {
    var API = docsAppProjService;
    return {
      getStatusCodes: function (callback) {
        return API.get('/api/statusCodes', callback);
      },
      getDrivers: function (callback) {
        return API.get('/api/drivers', callback);
      },
      getCabRequests: function (callback) {
        return API.get('/api/cabRequests', callback);
      },
      postCabRequest: function(body, callback) {
        return API.post('/api/cabRequests', body, callback);
      }
    };
  }
}());
