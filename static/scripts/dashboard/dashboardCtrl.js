(function () {
  'use strict';

  docsappproj.controller('dashboardCtrl', ['$scope', '$q', '$state',
    'apiAdapter', dashboardCtrl
  ])
  .config(['$stateProvider', 'SRC_PATH',
    function ($stateProvider, SRC_PATH) {
      $stateProvider.state('dashboard', {
        url: '/',
        templateUrl: SRC_PATH + 'dashboard/dashboard.html',
        controller: 'dashboardCtrl'
      });
    }
  ]);


  function dashboardCtrl($scope, $q, $state, apiAdapter) {
    var dashboardCtrlDefer = $q.defer();

    $scope._r.showCrumb = false;
    $scope.dashboardCtrlPromise = dashboardCtrlDefer.promise;

    $scope.vm = {
      isLoaded: false,
    };

    $scope._r.appPromise.then(initWorkflow);

    function initWorkflow() {
      var bag = {};

      async.series([
          setBreadcrumb.bind(null, bag),
          getStatusCodes.bind(null, bag),
          getDrivers.bind(null, bag),
          getCabRequests.bind(null, bag)
        ],
        function (err) {
          $scope.vm.isLoaded = true;
          if (err) {
            dashboardCtrlDefer.reject(err);
            console.log(err);
          }
          $scope.vm.driverMap =
            mapDriverStates(bag.cabRequests, bag.drivers);

          $scope.vm.drivers = bag.drivers;
          $scope.sampleData = [1, 2];
          dashboardCtrlDefer.resolve();
        }
      );
    }

    function setBreadcrumb(bag, next) {
      $scope._r.crumbList = [];

      var crumb = {
        name: 'Dashboard'
      };
      $scope._r.crumbList.push(crumb);
      $scope._r.showCrumb = true;
      $scope._r.title = 'DocsAppProj - Ola Queueing';
      return next();
    }

    function getStatusCodes(bag, next) {
      apiAdapter.getStatusCodes(
        function (err, statusCodes) {
          if (err)
            return next(err);
          bag.statusCodes = statusCodes;
          return next();
        }
      );
    }

    function getDrivers(bag, next) {
      apiAdapter.getDrivers(
        function (err, drivers) {
          if (err)
            return next(err);
          bag.drivers = drivers;
          return next();
        }
      );
    }

    function getCabRequests(bag, next) {
      apiAdapter.getCabRequests(
        function (err, cabRequests) {
          if (err)
            return next(err);
          bag.cabRequests = cabRequests;
          return next();
        }
      );
    }

    function mapDriverStates(cabRequests, drivers) {
      var result = {};

      drivers.forEach(
        function(driver) {
          result[driver.id] = [];
        }
      );

      cabRequests.forEach(
        function(cabReq) {
          if (cabReq.statusCode === 30) {
            result[cabReq.driverId].push(cabReq);
          } else if (cabReq.statusCode === 20) {
            result[cabReq.driverId].push(cabReq);
          } else {
              drivers.forEach(
                function(driver) {
                  result[driver.id].push(cabReq);
                }
              );
          }
        }
      );
      return result;
    }

    $scope.vm.postCabRequest = function(customerId) {
      if (!customerId) {
        alert('please enter a customerId');
        return;
      }

      var body = {
        customerId: customerId
      };

      apiAdapter.postCabRequest(body,
        function(err, cabReq){
          if (err)
            console.log(err);
          else
            console.log('succesfully posted cabreq:' + cabReq.id);
        }
      );
    };

    $scope.vm.mapCodeToText = function(code) {
      if (code === 10)
        return 'Waiting';
      else if (code === 20)
        return 'Ongoing';
      else if (code === 30)
        return 'Completed';
      else if (code === 40)
        return 'Free';
      else if (code === 50)
        return 'Busy';
      return 'Unknown';
    };

    $scope.vm.computeTimeDifference = function (time) {
      var now = new Date();
      var then = new Date(time);

      var duration = now - then;
      var seconds = parseInt((duration/1000)%60);
      var minutes = parseInt((duration/(1000*60))%60);
      var hours = parseInt((duration/(1000*60*60))%24);

      hours = (hours < 10) ? '0' + hours : hours;
      minutes = (minutes < 10) ? '0' + minutes : minutes;
      seconds = (seconds < 10) ? '0' + seconds : seconds;

        var string = '';
        if (hours > 0)
          string += hours + ' hours, ';
        if (minutes > 0)
          string += minutes + ' minutes, ';
        if (seconds > 0)
          string += seconds + ' seconds';
        return string;
    };

    $scope.vm.refresh = function () {
      initWorkflow();
    };

  }
}());
